package com.example.ideaintegrationapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toolbar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import in.juspay.godel.PaymentActivity;
import in.juspay.godel.data.JuspayResponseHandler;
import in.juspay.godel.ui.HyperPaymentsCallback;
import in.juspay.godel.ui.JuspayWebView;
import in.juspay.services.PaymentServices;

public class MainActivity extends AppCompatActivity {

    TextView message;
    String response;
    Boolean useBetaAssets;
    String clientId;
    String signaturePayloadX;
    PaymentServices juspayService;
    JSONObject signaturePayload;
    JSONObject orderDetails;
    String requestId;
    String orderId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        juspayService = new PaymentServices(this);
        this.message = findViewById(R.id.msg);

        Activity paymentActivity = new Activity();
        useBetaAssets = false;
        clientId = "idea_android";
        WebView.setWebContentsDebuggingEnabled(true);

        this.requestId = "X" + (long) (Math.random() * 10000000000L);
        this.orderId = "R" + (long) (Math.random() * 10000000000L);


        PaymentActivity.preFetch(this, clientId, useBetaAssets);


    }

    @Override
    public void onBackPressed() {
        boolean backPressHandled = juspayService.onBackPressed();
        if(!backPressHandled) {
            super.onBackPressed();
        }
    }

    public static String getTimeStamp(){
        return Long.toString(System.currentTimeMillis());
    }

    public void signSingaturePayload(View view){
        this.signaturePayload = new JSONObject();
        try {
            this.message.setText("Signing Signature...");
            this.signaturePayload.put("first_name", "Sahil");
            this.signaturePayload.put("last_name", "Sinha");
            this.signaturePayload.put("mobile_number", "9592329220");
            this.signaturePayload.put("email_address", "sahil.sinha@juspay.in");
            this.signaturePayload.put("customer_id", "9592329220");
            this.signaturePayload.put("time_stamp", getTimeStamp());
            this.signaturePayload.put("merchant_id", "idea_preprod");

            this.signaturePayloadX = this.signaturePayload.toString();
            new ApiCaller().execute();
        } catch (Exception e){
            this.message.setText(e.toString());
        }
    }

    public void initiateJuspaySdk(final View view) {
        JSONObject initiatePayload = new JSONObject();

        try{

            JSONObject payload = new JSONObject();

            payload.put("action", "payment_page");
            payload.put("clientId", "idea_android");
            payload.put("merchantKeyId", "2980");
            payload.put("signaturePayload", this.signaturePayload);
            payload.put("signature", this.response);
            payload.put("environment", "sandbox");
            payload.put("betaAssets", "false");

            initiatePayload.put("requestId", this.requestId);
            initiatePayload.put("service", "in.juspay.hyperpay");
            initiatePayload.put("payload", payload);

            Log.d("initiatePayload", initiatePayload.toString());

            juspayService.initiate(initiatePayload, new HyperPaymentsCallback() {
                @Override
                public void onStartWaitingDialogCreated(@Nullable View view) {

                }

                @Override
                public void onWebViewReady(JuspayWebView juspayWebView) {

                }

                @Override
                public void onEvent(JSONObject data, JuspayResponseHandler juspayResponseHandler) {
                    try {
                        String event = data.getString("event");
                        if(event.equals("show_loader")){
                            Log.d("jp","show_loader");
                        }
                        else if(event.equals("hide_loader")){
                            Log.d("jp", "hide_loader");
                        }
                        else if(event.equals("process_result")){
                            JSONObject response = data.optJSONObject("payload");
                            Log.d("jpResposne", response.toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e){
            message.setText(e.toString());
        }
    }

    public void updateMessage(View view){
        this.message.setText(this.response);
    }

    public void signOrderDetails(View view){
        this.orderDetails = new JSONObject();
        try {
            this.message.setText("Signing order details ....");
            this.orderDetails.put("order_id", this.orderId);
            this.orderDetails.put("amount", "1.0");
            this.orderDetails.put("customer_id", "9592329220");
            this.orderDetails.put("merchant_id", "idea_preprod");
            this.orderDetails.put("customer_email", "sahil.sinha@juspay.in");
            this.orderDetails.put("customer_phone", "9592329220");
            this.orderDetails.put("return_url", "");
            this.orderDetails.put("description", "Test Transaction");
            this.signaturePayloadX = orderDetails.toString();
            new ApiCaller().execute();
        } catch (Exception e){
            this.message.setText(this.response);
        }
    }

    public void startJuspaySdk(final View view){
        try{
            JSONObject processPayload = new JSONObject();
            JSONObject payload = new JSONObject();

            getSupportActionBar().hide();

            payload.put("action", "payment_page");
            payload.put("merchantId", "idea_preprod");
            payload.put("clientId", "idea_android");
            payload.put("orderId", this.orderId);
            payload.put("amount", "1.0");
            payload.put("customerId", "9592329220");
            payload.put("customerEmail", "sahil.sinha@juspay.in");
            payload.put("merchantKeyId", "2980");
            payload.put("language", "english");


            ArrayList<String> endUrlArr = new ArrayList<>(Arrays.asList("https://www.reload.in/recharge/", ".*www.reload.in/payment/f.*", ".*sandbox.juspay.in\\/thankyou.*", ".*wallet.juspay.dev:8080/recharge/payment.*", ".*www.foodstag.in\\/payment-gateway\\/handle-payment.*", ".*sandbox.juspay.in\\/end.*", ".*voonik.org\\/checkout\\/juspay_callback", ".*localhost.*", ".*api.juspay.in\\/end.*"));
            JSONArray endUrls = new JSONArray(endUrlArr);

            payload.put("endUrls", endUrls);

            payload.put("orderDetails", orderDetails.toString());

            payload.put("signature", this.response);

            processPayload.put("requestId",this.requestId);
            processPayload.put("service", "in.juspay.hyperpay");
            processPayload.put("payload", payload);

            juspayService.process(processPayload);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public class ApiCaller extends AsyncTask<URL, Integer, String> {

        protected String doInBackground(URL... urls) {

            String url = "https://dry-cliffs-89916.herokuapp.com/sign-idea?payload=";
            url += signaturePayloadX;

            try {
                URL httpsUrl = new URL(url);

                HttpsURLConnection connection = (HttpsURLConnection)httpsUrl.openConnection();

                if(connection != null){
                    Log.d("connectionRepsonse "+ connection.getResponseCode(), "");
                    String resp = evaluateConnectionResponse(connection);
                    Log.d("final Response", resp);


                    for(int i=0; i<10; i++)
                        publishProgress(i*10);

                    response = resp;
                    return resp;
                }


            } catch (Exception e) {
                return e.toString();
            }
            return "error";
        }

        public String evaluateConnectionResponse(HttpsURLConnection connection){

            try {
                int responseCode = connection.getResponseCode();
                InputStreamReader responseReader;
                if ((responseCode < 200 || responseCode >= 300) && responseCode != 302) {
                    responseReader = new InputStreamReader(connection.getErrorStream());
                } else {
                    responseReader = new InputStreamReader(connection.getInputStream());
                }

                BufferedReader in = new BufferedReader(responseReader);
                StringBuilder response = new StringBuilder();

                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                in.close();
                String responsePayload = response.toString();
                connection.disconnect();

                return responsePayload;

            } catch (Exception e){
                e.printStackTrace();
            }
            return "ErrorBoy";
        }

        protected void onProgressUpdate(Integer... progress) {
            message.setText("Done");
        }
        protected void onPostExecute(String result) {
            Log.d("onPostExecute", result);
        }

    }
}
